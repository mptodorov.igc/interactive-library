var $win = $(window);

var $slider = $('.js-slider')
var $sliderNav = $('.js-slider-prev, .js-slider-next')
var $sliderPrev = $('.js-slider-prev')
var $sliderNext = $('.js-slider-next')
var $sliderBG = $('.js-slider-bg')
var $gallery = $('.js-gallery')

// Sliders
$sliderNav.on('click', function(event) {
	event.preventDefault();

	var $this = $(this);
	var $activeSlide = $slider.find('.active');
	var $activePrev = $activeSlide.prev();
	var $activeNext = $activeSlide.next();

	if ($this.hasClass('js-slider-prev') && $activePrev.length) {
		$activeSlide
			.removeClass('active')
		$activePrev
			.addClass('active')
	} else if ($this.hasClass('js-slider-next') && $activeNext.length) {
		$activeSlide
			.removeClass('active')
		$activeNext
			.addClass('active')
	}

	$activeSlide = $slider.find('.active');
	
	if ($activeSlide.prev().length) {
		$sliderPrev.removeClass('hidden')
	} else {
		$sliderPrev.addClass('hidden')
	}

	if ($activeSlide.next().length) {
		$sliderNext.removeClass('hidden')
	} else {
		$sliderNext.addClass('hidden')
	}
});

if ($sliderBG.length) {
	var $sliderBGItems = $sliderBG.find('span');
	var sliderBGSpeed = $sliderBG.data('speed') ? $sliderBG.data('speed') : 3000;

	setInterval(function() {
		var $sliderBGCurrent = $sliderBG.find('.active').length ? $sliderBG.find('.active') : $sliderBG.find('span:first-child');

		if ($sliderBGCurrent.length) {
			var $sliderBGNext = $sliderBGCurrent.next().length ? $sliderBGCurrent.next() : $sliderBG.find('span:first-child');

			$sliderBGCurrent.removeClass('active');
			$sliderBGNext.addClass('active');
		}
	}, sliderBGSpeed)
}

// Manage popups
if ($gallery.length) {
	$gallery.magnificPopup({
		delegate: 'a',
		type: 'image',
		gallery: {
			enabled: true
		}
	});
}
